/**
 * usuario_condominio.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        usuario:{
            model:'usuarios',
            required:true
        },
        condominio:{
            model:'condominios',
            required:true
        }
    }
}