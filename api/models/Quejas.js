/**
 * Quejas.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        descripcion:{
            type:'string',
            required:true
        },
        propietario:{
            model:'propietarios',
            required:true
        },
        area:{
            model:'areas_sociales'
        },
        condominio:{
            model:'condominios',
            required:true
        },
        estado:{
            type:'string',
            required:true
        }
    }
}