/**
 * Estacionamientos.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        descripcion:{
            type:'string'
        },
        peticion:{
            type: 'ref', 
            columnType: 'datetime',
            required: true
        },
        inicio:{
            type: 'ref', 
            columnType: 'datetime'
        },
        fin:{
            type: 'ref', 
            columnType: 'datetime'
        },
        estado:{
            type:'string',
            isIn:['peticion','apartado','finalizado'],
            required: true
        },
        propietario:{
            model:'propietarios'
        },
        condominio:{
            model:'condominios'
        }
    }
}