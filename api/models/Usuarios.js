/**
 * Usuarios.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

	attributes: {
		nombre:{
			type:'string',
			required: true,
			maxLength: 120,
			unique: true,
		},
		apellido:{
			type:'string',
			required: true,
      		maxLength: 120,
		},
		usuario:{
			type:'string',
			required: true,
			maxLength: 120,
		},
		contrasena:{ //no salir en JSON
			type:'string',
			required: true,
			maxLength: 120,
		},
		descripcion:{
			type:'string'
		},
		correo:{
			type: 'string',
			unique: true,
			isEmail: true,
			maxLength: 200,
			required:true
		},
		telefono:{
			type:'string',
			maxLength: 20,
		},
		cedula:{
			type:'string',
			unique: true,
			maxLength: 20,
		},
		tipo:{
			model:'tipos_usuario',
			required:true
		}
	}
};

