/**
 * Usuarios
 *
 * @description :: Server-side logic for managing ciclos
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */


module.exports = {


  iniciar_sesion: function (req, res) {
    let user = req.param('usuario');
    let pass = req.param('contrasena');
    if (!user || !pass) {
      return res.jsonp({
        err: 'Datos incompletos',
        detail: 'Asegurate de llenar los campos, para seguir'
      });
    } else {
      Usuarios.findOne({ usuario: user }).exec((err, user1) => {
        if (err) {
          return res.json({
            err: '!Ups! algo ha ido mal',
            detail: 'Estamos trabajando en ello.'
          });
        }
        else if (!user1) {
          return res.json({ err: '!Ups!', detail: 'El usuario o la contraseña son incorrectos' });
        } else {
          if (pass != user1.contrasena) {
            return res.json({ err: '!Ups!', detail: 'El usuario o la contraseña son incorrectos' });
          } else {
            delete user1.contrasena;
            delete user1.createdAt;
            delete user1.updatedAt;
            return res.json(user1);
          }
        }
      });
    }
  },


};
