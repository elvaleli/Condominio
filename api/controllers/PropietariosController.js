/**
 * Propietarios
 *
 * @description :: Server-side logic for managing ciclos
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */


module.exports = {

  listar: (req, res) => {
    let sql = 'SELECT u.nombre, u.apellido, u.usuario, u.correo, u.telefono FROM propietarios as p, usuarios as u WHERE u.id=p.usuario ';
    Propietarios.query(sql, (err, data) => {
      console.log(data.rows);
      return res.json(data.rows);
    });
  },
};
