/**
 * Sails Seed Settings
 * (sails.config.seeds)
 *
 * Configuration for the data seeding in Sails.
 *
 * For more information on configuration, check out:
 * http://github.com/frostme/sails-seed
 */
module.exports.seeds = {
  tipos_usuario: [
    {
      nombre: 'Administrador'
    },
    {
      nombre: 'Propietario'
    },
    {
      nombre: 'Condominio'
    }
  ],
  usuarios: [
    {
      nombre: "Elian",
      apellido: "Zapata",
      usuario: "admin",
      contrasena: "1234",
      correo: "elvaleli@gmail.com",
      telefono: "3222003339",
      cedula: "1093456123",
      tipo: 1
    }, {
      nombre: "Pablo",
      apellido: "Mendoza",
      usuario: "pablo",
      contrasena: "1234",
      correo: "pablo@gmail.com",
      telefono: "31234521321",
      cedula: "10919565734",
      tipo: 2
    }, {
      nombre: "Jair",
      apellido: "Ortiz",
      usuario: "jair",
      contrasena: "1234",
      correo: "jair@gmail.com",
      telefono: "31342156734",
      cedula: "10923456674",
      tipo: 2
    }, {
      nombre: "Juan",
      apellido: "Perez",
      usuario: "juan",
      contrasena: "1234",
      correo: "juan@gmail.com",
      telefono: "3134324356",
      cedula: "10934546578",
      tipo: 2
    }
  ],
  propietarios: [
    {
      usuario: 2
    }, {
      usuario: 3
    }, {
      usuario: 4
    }
  ]
}
